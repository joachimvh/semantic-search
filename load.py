
import json
import httplib

conn = httplib.HTTPConnection('localhost', 9200)
conn.request('DELETE', '/defect')
resp = conn.getresponse()
print resp.read()
print resp.reason
print resp.status

# https://www.elastic.co/guide/en/elasticsearch/guide/current/relevance-is-broken.html
'''settings = {
    'settings': {
        'number_of_shards' : 1,
        'analysis' : {
            'analyzer' : {
                'default' : {
                    'type' : 'snowball',
                    'stopwords' : '_english_'                    
                }
            }
        },
        'similarity' : {
            'default' : {
                'type' : 'default'
            }
        }
    }
}'''

lowercase = {
    'type': 'lowercase'
}
stop = {
    'type': 'stop',
    'stopwords' : '_english_'
}
stemmer = {
    'type': 'stemmer',
    'language': 'english'
}
possessive_stemmer = {
    'type': 'stemmer',
    'language': 'possessive_english'
}
snowball = {
    'type': 'snowball',
    'language': 'English'
}
synonym = {
    'type': 'synonym',
    'format': 'wordnet',
    'synonyms_path': 'analysis/wn_s.pl'
}
filter = {
    'lowercase': lowercase,
    'stop': stop,
    'stemmer': stemmer,
    'possessive_stemmer': possessive_stemmer,
    'synonym': synonym
}

analyzer = {
    'default': {
        'tokenizer': 'standard',
        'filter': [ 'lowercase', 'stop', 'stemmer', 'possessive_stemmer', 'synonym' ]
    }
}

settings = {
    'settings': {
        'number_of_shards' : 1,
        'analysis': {
            'analyzer': analyzer,
            'filter': filter
        },
        'similarity': {
            'default': {
                'type': 'default'
            }
        }
    }
}

conn = httplib.HTTPConnection('localhost', 9200)
conn.request('PUT', '/defect', json.dumps(settings))
resp = conn.getresponse()
print resp.read()
print resp.reason
print resp.status

with open('data.json') as f:
    defects = json.load(f)

post_data = []
for defect in defects:
    post_data.append('{ "index" : { "_index" : "defect", "_type" : "defect", "_id" : "%s" } }' % defect['id'])
    post_data.append(json.dumps(defect))

conn = httplib.HTTPConnection('localhost', 9200)
conn.request('POST', '/_bulk', '\n'.join(post_data) + '\n')
resp = conn.getresponse()
print "%d items loaded into elasticsearch" % len(json.loads(resp.read())['items'])
print resp.reason
print resp.status