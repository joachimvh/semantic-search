/**
 * Created by joachimvh on 23/05/2016.
 */

var request = require('request');
var url = require('url');
var fs = require('fs');

function SemanticSearch (host, idx_name, options)
{
    this.host = host;
    this.idx_name = idx_name;
    this.options = {
        include: false,
        result_size: 3,
        min_term_freq: 1,
        min_doc_freq: 1
    };
    for (var key in (options || {}))
    {
        if (!(key in this.options))
            throw new Error("Unsupported option: " + key);
        this.options[key] = options[key];
    }
}

SemanticSearch.prototype._entry_path = function (id)
{
    return '/' + this.idx_name + '/' + this.idx_name + '/' + id;
};

SemanticSearch.prototype.info = function (id, callback)
{
    if (!callback)
    {
        callback = id;
        id = undefined;
    }
    if (id)
        request.get(url.resolve(this.host, this._entry_path(id)), callback);
    else
        request.get(url.resolve(this.host, this.idx_name), callback);
};

SemanticSearch.prototype.delete_index = function (callback)
{
    request.delete(url.resolve(this.host, this.idx_name), callback);
};

SemanticSearch.prototype.delete_entry = function (id, callback)
{
    request.delete(url.resolve(this.host, this._entry_path(id)), callback);
};

SemanticSearch.prototype.setup_index = function (callback)
{
    // https://www.elastic.co/guide/en/elasticsearch/guide/current/relevance-is-broken.html
    var settings = {
        settings: {
            number_of_shards: 1, // TF-IDF only gets calculated per shard!
            number_of_replicas: 0,
            analysis: {
                analyzer: {
                    default: {
                        type: 'snowball',      // has stemming
                        stopwords: '_english_' // already included by default in snowball actually
                    }
                }
            },
            similarity: {
                default: {
                    type: 'default' // just in case we want to change it at one point
                }
            }
        }
    };
    request.put({url: url.resolve(this.host, this.idx_name), json: settings}, callback);
};

SemanticSearch.prototype.set_entry = function (id, body, callback)
{
    // https://www.elastic.co/guide/en/elasticsearch/guide/current/update-doc.html
    request.put({url: url.resolve(this.host, this._entry_path(id)), json: body}, callback);
};

SemanticSearch.prototype.setup_index_synonyms = function (callback)
{
    var lowercase = { type: 'lowercase' };
    var stop = { 'type': 'stop', stopwords: '_english_' };
    var stemmer = { type: 'stemmer', language: 'english' };
    var possessive_stemmer = { type: 'stemmer', language: 'possessive_english' };
    var synonym = { type: 'synonym', 'format': 'wordnet', synonyms_path: 'analysis/wn_s.pl' };

    var filter = {
        lowercase: lowercase,
        stop: stop,
        stemmer: stemmer,
        possessive_stemmer: possessive_stemmer,
        synonym: synonym
    };
    var analyzer = {
        default: {
            tokenizer: 'standard',
            filter: [ 'lowercase', 'stop', 'stemmer', 'possessive_stemmer', 'synonym' ]
        }
    };

    var settings = {
        settings: {
            number_of_shards: 1, // TF-IDF only gets calculated per shard!
            number_of_replicas: 0,
            analysis: {
                analyzer: analyzer,
                filter: filter
            }
        }
    };
    request.put({url: url.resolve(this.host, this.idx_name), json: settings}, callback);
};

SemanticSearch.prototype.bulk_add = function (entries, callback)
{
    var data = [];
    for (var i = 0; i < entries.length; ++i)
    {
        var entry = entries[i];
        var metadata = {
            index: {
                _index: this.idx_name,
                _type: this.idx_name
            }
        };
        if (entry.id)
            metadata.index._id = entry.id;
        data.push(JSON.stringify(metadata), JSON.stringify(entry));
    }
    // extra newline is necessary
    var postdata = data.join('\n') + '\n';
    request.post({url: url.resolve(this.host, '/_bulk'), body: postdata}, callback);
};

SemanticSearch.prototype.flush = function (callback)
{
    request.post({url: url.resolve(this.host, this.idx_name + '/_flush')}, callback);
};

// fields can't be used when the input is a doc?
SemanticSearch.prototype.more_like_this_doc = function (doc, callback)
{
    var mlt = {
        size: this.options.result_size,
        query: {
            more_like_this: {
                like: [{
                    _index: this.idx_name,
                    _type: this.idx_name,
                    doc: doc
                }],
                min_term_freq: this.options.min_term_freq, // how many times a term must occur before it is relevant
                min_doc_freq: this.options.min_doc_freq,   // how many documents need to contain a term for it to be relevant
                include: this.options.include              // include requested document in results
            }
        }
    };
    request.post({url: url.resolve(this.host, '/' + this.idx_name + '/_search'), json: mlt}, callback);
};

SemanticSearch.prototype.more_like_this = function (id, fields, callback)
{
    var mlt = {
        size: this.options.result_size,
        query: {
            more_like_this: {
                fields: fields,
                like: [{
                    _index: this.idx_name,
                    _type: this.idx_name,
                    _id: id
                }],
                min_term_freq: this.options.min_term_freq, // how many times a term must occur before it is relevant
                min_doc_freq: this.options.min_doc_freq,   // how many documents need to contain a term for it to be relevant
                include: this.options.include              // include requested document in results
            }
        }
    };
    
    // reset to default if no fields are provided
    if (fields.length === 0)
        delete mlt.query.more_like_this.fields;
    
    request.post({url: url.resolve(this.host, '/' + this.idx_name + '/_search'), json: mlt}, callback);
};

SemanticSearch.prototype.more_like_this_extended = function (id, fields, weights, callback)
{
    if (fields.length !== weights.length)
        throw new Error('# of fields !== # of weights');
    
    // not making use of extended functionality
    if (fields.length === 0 || weights.every(function (val) { return val === 1; }))
        return this.more_like_this(id, fields, callback);
    
    var mlt = {
        size: this.options.result_size,
        query: {
            bool: {
                should: []
            }
        }
    };

    for (var i = 0; i < fields.length; ++i)
    {
        mlt.query.bool.should.push({
            more_like_this: {
                fields: [fields[i]],
                like: [{
                    _index: this.idx_name,
                    _type: this.idx_name,
                    _id: id
                }],
                min_term_freq: this.options.min_term_freq, // how many times a term must occur before it is relevant
                min_doc_freq: this.options.min_doc_freq,   // how many documents need to contain a term for it to be relevant
                include: this.options.include,             // include requested document in results
                boost: weights[i]
            }
        });
    }

    request.post({url: url.resolve(this.host, '/' + this.idx_name + '/_search'), json: mlt}, callback);
};

module.exports = SemanticSearch;