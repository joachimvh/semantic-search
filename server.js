
var _ = require('lodash');
var express = require('express');
var bodyParser = require('body-parser');
var SemanticSearch = require('./SemanticSearch');

var args = require('minimist')(process.argv.slice(2));
if (args.h || args.help || args._.length !== 1 || !_.isEmpty(_.omit(args, ['_', 'p'])))
{
    console.error('usage: node server.js ELASTICSEARCH_URL [-p port] [--help]\nelasticsearch url is probably http://localhost:9200');
    return process.exit((args.h || args.help) ? 0 : 1);
}

var port = args.p || 3000;
var elastic = args._[0];

var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies

// only accept json
app.use(function(req, res, next) {
    var contype = req.headers['content-type'];
    if ((req.method === 'POST' || req.method === 'PUT')&& (!contype || contype.indexOf('application/json') !== 0))
        return res.status(406).json({error: 'Only accepting JSON content.'});
    next();
});

function parseJSON(input)
{
    return _.isString(input) ? JSON.parse(input) : input;
}

function createDefaultCallback(res)
{
    return function (error, response, body)
    {
        if (error)
            res.status(500).json(parseJSON(error));
        else
            res.status(200).json(parseJSON(body));
    }
}

app.get('/:index', function (req, res)
{
    var index = req.params.index;
    var search = new SemanticSearch(elastic, index);
    search.info(createDefaultCallback(res));
});

app.put('/:index', function (req, res)
{
    var index = req.params.index;
    var search = new SemanticSearch(elastic, index);
    var synonyms = req.body.synonyms;
    search.delete_index(function (error, response, body)
    {
        if (error)
            return res.status(500).json(parseJSON(error));
        
        if (synonyms)
            search.setup_index_synonyms(createDefaultCallback(res));
        else
            search.setup_index(createDefaultCallback(res));
    });
});

app.post('/:index', function (req, res)
{
    var index = req.params.index;
    var body = req.body;
    // if (!body.id)
    //     return res.status(400).send('"id" field required.');
    if (!body || !_.isArray(body))
        return res.status(400).json({error: 'Input should be a list of entries.'});
    
    var search = new SemanticSearch(elastic, index);
    search.bulk_add(body, function (error, response, body)
    {
        if (error)
            res.status(500).json(parseJSON(error));
        else
            search.flush(function (error)
            {
                if (error)
                    res.status(500).json(parseJSON(error));
                else
                    res.status(200).json(parseJSON(body)); // send bulk_add body for more information
            });
    });
});

app.delete('/:index', function (req, res)
{
    var index = req.params.index;
    var search = new SemanticSearch(elastic, index);
    search.delete_index(createDefaultCallback(res));
});

app.post('/:index/similar', function (req, res)
{
    var index = req.params.index;
    
    var body = req.body;
    if (!body.entry)
        return res.status(400).json({error: '"entry" and "fields" required.'});
    
    var entry = body.entry;
    
    var options = body.options || {};
    var search = new SemanticSearch(elastic, index); // create dummy object to get default option values
    var valid_options = Object.keys(search.options);
    for (var key in options)
        if (valid_options.indexOf(key) < 0)
            return res.status(400).json({error: 'Invalid option ' + key + '. Valid options are ' + JSON.stringify(valid_options)});
    search = new SemanticSearch(elastic, index, options);
    
    search.more_like_this_doc(entry,createDefaultCallback(res));
});

app.get('/:index/:id', function (req, res)
{
    var index = req.params.index;
    var id = req.params.id;
    var search = new SemanticSearch(elastic, index);
    search.info(id, function (error, response, body)
    {
        if (error)
            res.status(500).json(parseJSON(error));
        else if (!parseJSON(body).found)
            res.sendStatus(404);
        else
            res.status(200).json(parseJSON(body));
    });
});

app.put('/:index/:id', function (req, res)
{
    var index = req.params.index;
    var id = req.params.id;
    var search = new SemanticSearch(elastic, index);
    search.set_entry(id, req.body, createDefaultCallback(res));
});

app.delete('/:index/:id', function (req, res)
{
    var index = req.params.index;
    var id = req.params.id;
    var search = new SemanticSearch(elastic, index);
    search.delete_entry(id, createDefaultCallback(res));
});

app.post('/:index/:id/similar', function (req, res)
{
    var index = req.params.index;
    var id = req.params.id;
    
    var body = req.body;
    if (!body.fields)
        body.fields = [];
    if (!_.isArray(body.fields))
        return res.status(400).json({error: '"fields" should be an array.'});
    
    var fields = body.fields;
    var weights = body.weights;
    
    if (!weights)
        weights = _.range(1, fields.length+1, 0);
    
    var options = body.options || {};
    var search = new SemanticSearch(elastic, index); // create dummy object to get default option values
    var valid_options = Object.keys(search.options);
    for (var key in options)
        if (valid_options.indexOf(key) < 0)
            return res.status(400).json({error: 'Invalid option ' + key + '. Valid options are ' + JSON.stringify(valid_options)});
    search = new SemanticSearch(elastic, index, options);
    
    search.more_like_this_extended(id, fields, weights, createDefaultCallback(res));
});

app.listen(port);