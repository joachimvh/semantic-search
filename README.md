# Semantic Search

The Semantic Search interface allows users to search for similar documents. More information about the inner workings can be found in deliverable 3.2.

All `POST` and `PUT` requests require JSON (`Content-Type: application/json`) as input.

# Setting up Docker

To run the Semantic Search container simply execute `docker-compose up` in the root folder and execute `docker ps` to see the linked port.

# Getting started

This section explains the steps that need to be taken to set up an elasticsearch index to use for the semantic search.

## Initialize

The first thing that needs to happen is setting up an index using a `PUT` command. This will configure the given index for semantic search (optionally with synonyms if requested.)
Example:
```HTTP
PUT /defects HTTP/1.1
Host: localhost:3000
Content-type: application/json

{"synonyms": true}
```

If this index already exists it will be overwritten and all contained entries will be deleted.

## Add data

Once the index has been set up, data can be added by `POST`ing a list to the index.
Example:
```HTTP
POST /defects HTTP/1.1
Host: localhost:3000
Content-type: application/json

[{
    "id": "a8fde43e-22c5-438d-bdc7-1ad97cf2576f",
    "name": "Flow Lines",
    "desc": "streaks, patterns, or lines off-toned in color - that show up on the prototype",
    "comment": "I see that the flow lines are mostly on the upper part of the product, "
},
{

    "id": "dcd4445e-917d-4168-a1b0-885a0dda4b66",
    "name": "Sink Marks",
    "desc": "small craters or depressions in thicker areas of the injection molded prototype.",
    "comment": "the sink is critical only when it is a visually evident effect"
}]
```

Example response:
```json
{
  "took":294,
  "errors":false,
  "items":[
    {
      "index":{
        "_index":"defects",
        "_type":"defects",
        "_id":"a8fde43e-22c5-438d-bdc7-1ad97cf2576f",
        "_version":1,
        "_shards":{
          "total":1,
          "successful":1,
          "failed":0
        },
        "status":201
      }
    },
    {
      "index":{
        "_index":"defects",
        "_type":"defects",
        "_id":"dcd4445e-917d-4168-a1b0-885a0dda4b66",
        "_version":1,
        "_shards":{
          "total":1,
          "successful":1,
          "failed":0
        },
        "status":201
      }
    }
  ]
}
```

## Perform search

Once the data has been added, the index can be used for finding similar documents by `POST`ing to the `similar` API.
Example:
```HTTP
POST /defects HTTP/1.1
Host: localhost:3000
Content-type: application/json

{
    "entry": {
        "desc": "there are craters in the prototype"
    },
    "options": {
        "result_size": 10
    }
}
```

Example response with a single match:
```json
{
  "took":49,
  "timed_out":false,
  "_shards":{
    "total":1,
    "successful":1,
    "failed":0
  },
  "hits":{
    "total":1,
    "max_score":6.861195,
    "hits":[
      {
        "_index":"defects",
        "_type":"defects",
        "_id":"a8fde43e-22c5-438d-bdc7-1ad97cf2576f",
        "_score":6.861195,
        "_source":{
          "id":"a8fde43e-22c5-438d-bdc7-1ad97cf2576f",
          "name":"Flow Lines",
          "desc":"streaks, patterns, or lines off-toned in color - that show up on the prototype",
          "part_id":"1",
          "media_url":"http://machinesandparts.surge.sh/part1/part1_flow_lines.png",
          "comment":"I see that the flow lines are mostly on the upper part of the product, in the farther position respect to the flow gate",
          "taxonomy":null
        }
      }
    ]
  }
}
```


# Index API

## Index info [`GET /:index`]
Returns info about the given index.

## Initialize index [`PUT /:index`]
Set up an index with the given name

| Variable | Type | Mandatory | Default | Description |
| -------- | ---- | :-------: | ------- | ------------|
| synonyms | bool | ✘        | false   | If the index should make use of synonyms for similarity.

Response:
```json
{"acknowledged":true}
```

## Populate index [`POST /:index`]
Add a list of entries to the index. There are no restrictions on what the entries can look like, but they all need to have the same fields. The body of this call needs to be a JSON list of the entries.
Note: if entries have an `id` field, this field will be used as identifier for the entries. If not identifiers will be automatically generated.

Response:

| Variable | Type  | Description |
| -------- | ----- | ------------|
| took     | int   | How long the operation took to execute.
| errors   | bool  | If there were any errors during the operation.
| items    | array | A list containing metadata for every added entry

Every item in the `items` array contains the following metadata:

| Variable       | Type   | Description |
| ------------   | ------ | ------------|
| index          | object | Root metadata object.
| index._index   | string | Index name.
| index._type    | string | Index name (types are not used for the semantic search).
| index._id      | string | Entry ID (either the given id or a newly generated one).
| index._version | int    | Version of the entry (1 if newly created, higher if replacing an existing entry).
| index._shards  | object | Shard metadata (only 1 shard is used for semantic search).
| index.status   | int    | Status response when creating the entry (201 if succesful).

## Delete index [`DELETE /:index`]
Deletes the given index completely. The index will need to be initialized again to be used

Response:
```json
{"acknowledged":true}
```

## Find similar documents [`POST /:index/similar`]
This call will find documents stored in the index that are similar to the given input. Additional options can be provided to finetune the results.

| Variable | Type   | Mandatory | Default     | Description |
| -------- | ------ | :-------: | ----------- | ------------|
| entry    | object | ✔        |             | The object that needs to be compared to. 
| options  | object | ✘        | (see below) | Additional options for the search action.

The following options are supported:

| Variable      | Type    | Mandatory | Default | Description |
| ------------- | ------- | :-------: | ------- | ------------|
| include       | bool    | ✘        | false   | If the original document can be one of the results.
| result_size   | integer | ✘        | 3       | The maximum number of results.
| min_term_freq | integer | ✘        | 1       | Terms that occur less than this in the input document will be ignored.
| min_doc_freq  | integer | ✘        | 1       | Terms that occur in less documents than this will be ignored.

Response:

| Variable       | Type   | Description |
| --------       | -----  | ------------|
| took           | int    | How long the operation took to execute.
| timed_out      | bool   | If the operation timed out
| _shards        | object | Shard metadata (only 1 shard is used for semantic search)
| hits           | object | Result object.
| hits.total     | int    | The total number of results found. The actual returned amount is dependend on the `result_size` option.
| hits.max_score | float  | The score of the best matching result.
| hits.hits      | array  | The list of results.

Every item in the `hits.hits` array has the following structure:

| Variable | Type   | Description |
| -------- | ------ | ------------|
| _index   | string | Index name.
| _type    | string | Index name (types are not used for the semantic search).
| _id      | string | Entry ID (either the given id or a newly generated one).
| _score   | float  | The similarity score.
| _source  | object | The similar entry.

# Entry API

## Entry info [`GET /:index/:id`]
Returns info about the given entry.

Response:

| Variable | Type   | Description |
| -------- | ------ | ------------|
| _index   | string | Index name.
| _type    | string | Index name (types are not used for the semantic search).
| _id      | string | Entry ID (either the given id or a newly generated one).
| _version | int    | The version of the entry can be higher than 1 if it was modified at some point.
| found    | bool   | Will always be true.
| _source  | object | The entry data.

## Update entry [`PUT /:index/:id`]
Updates the given entry.
The entry will be overwritten so the body should contain the entire new entry.

| Variable | Type   | Description |
| -------- | ------ | ------------|
| _index   | string | Index name.
| _type    | string | Index name (types are not used for the semantic search).
| _id      | string | Entry ID (either the given id or a newly generated one).
| _version | int    | The new version of the entry.
| _shards  | object | Shard metadata.
| created  | bool   | True if a new object was created. False if there already was an object with this id.

## Delete entry [`DELETE /:index/:id`]
Deletes the given entry.

| Variable | Type   | Description |
| -------- | ------ | ------------|
| found    | bool   | True if the given entry could be found (and deleted).
| _index   | string | Index name.
| _type    | string | Index name (types are not used for the semantic search).
| _id      | string | Entry ID (either the given id or a newly generated one).
| _version | int    | The version of the deleted entry.
| _shards  | object | Shard metadata.

## Find similar documents [`POST /:index/:id/similar`]
This works identical to finding similar documents over an index, only in this case an entry is no longer required. Since everything else is optional, an empty object is a valid input here.

| Variable | Type   | Mandatory | Default     | Description |
| -------- | ------ | :-------: | ----------- | ------------|
| fields   | array  | ✘        | []          | Only the fields in the given array will be used for the similarity comparison.
| weights  | array  | ✘        | []          | The weights of the given fields.
| options  | object | ✘        | (see above) | Additional options for the search action.

If used, `fields` should be an array of strings, corresponding to the keys of the values that need to be used for similarity comparison, e.g. `["title", "description"]`.
`weights` needs to be an array of floats with the same length as the `fields` array. Every value is the weight of the corresponding field, making it possible to put more importance on the value of certain fields. The default value is `1`.

Response:
Identical format to the output when performing the search on the index.
