
import json
import httplib

"""
mlt = {
    'size' : 33,
    'query' : {
        'more_like_this' : {
            'fields' : ['name', 'desc'],
            'like': [{
                '_index' : 'defect',
                '_type' : 'defect',
                '_id' : '9b79279f-419e-45e0-80df-46ac707ff84b'
            }],
            'min_term_freq' : 1,
            'min_doc_freq' : 1,
            'include' : True
        }
    }
}"""

mlt = {
    'size' : 33,
    'query' : {
        'bool': {
            'should': [
                {
                    'more_like_this' : {
                        'fields' : ['name'],
                        'like': [{
                            '_index' : 'defect',
                            '_type' : 'defect',
                            '_id' : '0090048d-3e52-4847-a29c-c4f297ca37ae'
                        }],
                        'min_term_freq' : 1,
                        'min_doc_freq' : 1,
                        'include' : True,
                        'boost': 1
                    }
                },
                {
                    'more_like_this' : {
                        'fields' : ['desc'],
                        'like': [{
                            '_index' : 'defect',
                            '_type' : 'defect',
                            '_id' : '0090048d-3e52-4847-a29c-c4f297ca37ae'
                        }],
                        'min_term_freq' : 1,
                        'min_doc_freq' : 1,
                        'include' : True
                    }
                }
            ]
        }
    }
}

conn = httplib.HTTPConnection('localhost', 9200)
conn.request('POST', '/_search', json.dumps(mlt))
resp = conn.getresponse()
print json.dumps(json.loads(resp.read()), indent=2)