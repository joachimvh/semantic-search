FROM node:latest

ARG port
ARG elasticsearch_url

RUN apt-get update && \
    apt-get install -y git

# http://slash-dev-blog.me/docker-git.html
# Fixes empty home
ENV HOME /root
# add custom ssh config / keys to the root user
COPY ssh/ /root/.ssh/
# Fixes permission if needed
RUN chmod 600 /root/.ssh/*
# Avoid first connection host confirmation
RUN ssh-keyscan -p4444 git.datasciencelab.ugent.be > /root/.ssh/known_hosts

# clone git
RUN git clone git@git.datasciencelab.ugent.be:facts4workers/semantic-search.git /opt/semantic-search 
WORKDIR /opt/semantic-search 
RUN npm install --quiet 

EXPOSE ${port:-3000}
ENTRYPOINT node server.js ${elasticsearch_url} -p ${port:-3000}
